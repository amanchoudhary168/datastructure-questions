package com.freecharge.ds;

import java.util.ArrayList;
import java.util.List;

public class ProductSum {
	public static void main(String[] args) {
		List<Object> input = new ArrayList<>();
		List<Object> level1 = new ArrayList<Object>();
		List<Object> level2 = new ArrayList<Object>();
		List<Object> level3 = new ArrayList<Object>();
		List<Object> level4 = new ArrayList<Object>();
		level4.add(5);
		level3.add(level4);
		level2.add(level3);
		level1.add(level2);
		input.add(level1);
		System.out.println(input);
		
		System.out.println(productSum(input));
	
	}
	public static int productSum(List<Object> array) {
	    // Write your code here.
	    return productSum(array,1);
	  }
		
		private static int productSum(List<Object> array , int multiplier){
			Integer sum =0;
				for(Object obj : array){
					if(obj instanceof Integer){
						sum+= (Integer) obj;
					}
					else{
						sum+= productSum((List<Object>)obj,multiplier+1);
					}
			}
			return sum*multiplier;
		}

}
