package com.freecharge.ds;

public class LongestSubStrWithoutRepitions {

    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstring("abcabcbb"));
    }

    public static int lengthOfLongestSubstring(String s) {
        int chars[] = new int[26];
        if(s.length()==0)
            return 0;

        int start=0;
        int end=0;
        int maxLength =0;
        boolean removeDup = true;


        while(end<s.length()){
            chars[s.charAt(end)-'a']+=1;

            while(true){
                boolean duplicateExists = false;
                for(int i=0;i<26;i++) {
                    if (chars[i] > 1) {
                        chars[s.charAt(start) - 'a'] = chars[s.charAt(start) - 'a'] > 0 ? chars[s.charAt(start) - 'a'] - 1 : 0;
                        start++;
                        duplicateExists = true;
                        break;
                    }
                }
                if(duplicateExists)
                    continue;
                else
                    break;

            }
            maxLength = Math.max(maxLength,(end-start)+1);
            end++;


        }
        return maxLength;
    }
}
