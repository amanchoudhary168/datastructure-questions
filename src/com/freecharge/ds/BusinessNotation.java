package com.freecharge.ds;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class BusinessNotation {


    public static void main(String[] args) {
        TreeMap<Double,String> unitMap = new TreeMap<>();
        unitMap.put(1000000D,"M");
        unitMap.put(1000D,"K");


        System.out.println(convertIntoBussinessNotation(2000000,unitMap));
    }


    public static String convertIntoBussinessNotation(double num, TreeMap<Double,String> unitMap){
        Set<Map.Entry<Double,String>> unitEntryList = unitMap.entrySet();
        //String notation = "";
        BigDecimal number = new BigDecimal(num);
        Map.Entry<Double,String> unitToApply =null;
        for(Map.Entry<Double,String> entry : unitEntryList){
            if(num/entry.getKey()<1){
                break;
            }
            unitToApply= entry;
        }
        BigDecimal unit = new BigDecimal(unitToApply.getKey());
        return number.divide(unit).toString()+" "+unitToApply.getValue();

    }
}
