package com.freecharge.ds;

import java.util.HashMap;

public class DecodeString {

    static HashMap<Integer,Integer> count = new HashMap<>();
    public static void main(String[] args) {
        //1836311903 ---13059
        String s = "111111111111111111111111111111111111111111111";
        //String s = "123";
        long startTime = System.currentTimeMillis();
        System.out.println(count(-1,s,-1));
        System.out.println("Time taken to execute ="+(System.currentTimeMillis()-startTime));
        //System.out.println(numDecodings(s));
    }


        public static int count(int start,String s,int prev){
        if(count.containsKey(start))
               return count.get(start);

        if(s.length()-1<start)
                return 0;

            int val =0;
            if(start-prev>1){
                int ones = s.charAt(start)-'0';
                int tens = s.charAt(start-1)-'0';
                if((tens*10)+(ones) >26)
                    return 0;
            }
            if(start==s.length()-1)
                return 1;

            if(s.charAt(start+1) =='0')
                return 0;
            int res  = count(start+1,s,start)+count(start+2,s,start);
            count.put(start,res);
            return res ;
        }

    public static int numDecodings(String s) {
        if(s.length()<1)
            return 0;

        if(s.length() == 1){
            if(s.charAt(0) == 0)
                return 0;
            return 1;
        }

        int val2=1;
        int val1=1;

        for(int i=1;i<s.length();i++){
            int val = 0;
            int d1 = s.charAt(i)-'0';
            int d2 = ((s.charAt(i-1) -'0') *10) + d1;

            if(d1>0)
                val+= val2;
            if(d2>9 && d2<27)
                val+= val1;

            val1=val2;
            val2 = val;
        }

        return val2;

    }
    }

