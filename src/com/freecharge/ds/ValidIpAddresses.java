package com.freecharge.ds;

import java.util.*;

class ValidIpAddresses {
	
	
	public static void main(String[] args) {
		//char ch = '2';
		String arr[] = "19.216.8.0".split("\\.");
		
		System.out.println(validIPAddresses("1921680"));
	}

  public static ArrayList<String> validIPAddresses(String string) {
    ArrayList<String> list = ipUtil(string,string.length()-1,1,"");
    return list;
  }
	
	public static ArrayList<String> ipUtil(String s,int start,int level,String ipString){
		
		int temp = 0;
		int pow = 1;
		int val=0;
		ArrayList<String> list = new ArrayList<String>();
		while(true && val<=255 && start-temp>=0 && temp<3){
			if(s.charAt(start-temp) =='0' && temp != 0){
				temp++;
				continue;
			}else{
				val = (s.charAt(start-temp)-'0')*pow+val;
				
			}
			pow = pow*10;
			
			if(start-temp == 0) {
				String finalIpAddress = val+ipString;
				if(finalIpAddress.split("\\.").length == 4)
					list.add(finalIpAddress);
			}
				
			temp++;
			ArrayList<String> ipList = new ArrayList<String>();
			if(level <= 3) {
				if(val<=255)
					ipList = ipUtil(s,start-temp,level+1,"."+val+ipString);
			}
			list.addAll(ipList);
			
		}
		return list;
	}
	
	
	 public static ArrayList<String> validIPAddress(String string) {
		    ArrayList<String> ipAddresses = new ArrayList<>();
				for(int i=1;i<Math.min(string.length(),4);i++){
					String ipParts[] = new String[]{"","","",""};
					ipParts[0] = string.substring(0,i);
					if(!isValidIp(ipParts[0])){
						continue;
					}
					for(int j = i+1;j<Math.min(string.length()-i,4);j++){
						ipParts[1] = string.substring(i,j);
						if(!isValidIp(ipParts[2])){
							continue;
						}
					
					for(int k = j+1;j<Math.min(string.length()-j,4);k++){
						ipParts[2] = string.substring(j,k);
						ipParts[3] = string.substring(k);
						if(isValidIp(ipParts[3]) && isValidIp(ipParts[4])){
							ipAddresses.add(join(ipParts));
						}
					}
				}
			}
		    return ipAddresses;
				
		}
			
			public static String join(String arr[]){
				StringBuilder sb = new StringBuilder();
				for(int l=0;l<arr.length;l++){
					sb.append(arr[l]);
					if(l<arr.length - 1){
						sb.append(".");
					}
				}
				return sb.toString();
			}
			public static boolean isValidIp(String str){
				int strAsInt = Integer.parseInt(str);
				if(strAsInt>255){
					return false;
				}
				return str.length() == Integer.toString(strAsInt).length();
			}
}
