package com.freecharge.ds;

import java.util.Arrays;

public class CountInversions {

    public static void main(String[] args) {
        int arr[] = {2, 3, 4, 5, 6};
        int noOfInversions = modifiedMergeSort(arr,0,arr.length-1);
        System.out.println(noOfInversions);
    }


    public static int modifiedMergeSort(int arr[],int l,int r){
        int count = 0;
        if(l<r){
            int m = (l+r)/2;
            count+= modifiedMergeSort(arr,l,m);
            count+= modifiedMergeSort(arr,m+1,r);
            count+= merge(arr,l,m,m+1,r);

        }
        return count;
    }

    public static int merge(int arr[],int s1,int e1,int s2,int e2){
        int ic = 0;
        int left[] = Arrays.copyOfRange(arr,s1,e1+1);
        int right[] = Arrays.copyOfRange(arr,s2,e2+1);
        int k=s1,i=0,j=0,swap = 0;
        while(i<left.length && j< right.length){
            if(left[i]<right[j]){
                arr[k++] = left[i++];
            }else{
                arr[k++] = right[j++];
                swap += left.length - i;
            }
        }
        while(i<left.length)
            arr[k++] = left[i++];
        while(j<right.length)
            arr[k++] = right[j++];
        return swap;
    }

}
