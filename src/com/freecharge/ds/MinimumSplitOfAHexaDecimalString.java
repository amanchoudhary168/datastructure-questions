package com.freecharge.ds;
import java.util.Arrays;

public class MinimumSplitOfAHexaDecimalString {
	
	public static void main(String[] args) {
		//String s = "101101101";
		String s =   "896bb1";  //"1a919";
	//	System.out.println(minCutsBinary(s,s.length()));
		System.out.println(findMinimumSplit(s));
	}
	
	
	
	public static int findMinimumSplit(String str) {
		int dp[] = new int[str.length()+1];
		Arrays.fill(dp, str.length());
		dp[0] = 0;
		int numFirst =  findNumericNumber(str,0,0);
		if(Math.sqrt(numFirst)%1 == 0) {
			dp[0] = 1;
		}
		
		
		
		for(int j=1;j<str.length();j++) {
			
			for(int k=0;k<j;k++) {
				int hexNumber = findNumericNumber(str,k,j);	
				System.out.println("The numeric value for hex string = "+hexNumber);
				double  num = Math.sqrt(hexNumber);
				System.out.println("Sqrt of hex string = "+num);
				if(num%1 != 0) {
					continue;
				}
				if(k!= 0)
					dp[j] = Math.min(dp[j],dp[k-1]+1);	
				else
					dp[j] = 1;
			}	
		}
		return ((dp[str.length()-1]<str.length())?dp[str.length()-1]:-1);
	}
	
	
	
	public static int findNumericNumber(String str,int start,int end) {
		String s = "";
		for(int i= start;i<=end;i++) {
			s = s+str.charAt(i);
		}
		Integer value = Integer.valueOf(s,16);
		return value;
		
	}
	
	 static int minCutsBinary(String s, int n)
	    {
	        int[] dp = new int[n + 1];
	 
	        // Alongocating memory for dp[] array
	        Arrays.fill(dp, n+1);
	        dp[0] = 0;
	 
	        // From length 1 to n
	        for (int i = 1; i <= n; i++)
	        {
	 
	            // If previous character is '0' then ignore
	            // to avoid number with leading 0s.
	            if (s.charAt(i - 1) == '0')
	            {
	                continue;
	            }
	            for (int j = 0; j < i; j++)
	            {
	 
	                // Ignore s[j] = '0' starting numbers
	                if (s.charAt(j) == '0')
	                {
	                    continue;
	                }
	 
	                // Number formed from s[j....i]
	                long num = number(s, j, i);
	 
	                // Check for power of 5
	                if (!ispower(num))
	                {
	                    continue;
	                }
	 
	                // Assigning min value to get min cut possible
	                dp[i] = Math.min(dp[i], dp[j] + 1);
	            }
	        }
	 
	        // (n + 1) to check if all the Strings are traversed
	        // and no divisible by 5 is obtained like 000000
	        return ((dp[n] < n + 1) ? dp[n] : -1);
	    }
	 
	
	static boolean ispower(long n)
    {
        if (n < 125)
        {
            return (n == 1 || n == 5 || n == 25);
        }
        if (n % 125 != 0)
        {
            return false;
        }
        else
        {
            return ispower(n / 125);
        }
    }
	
	
	static long number(String s, int i, int j)
    {
        long ans = 0;
        for (int x = i; x < j; x++)
        {
            ans = ans * 2 + (s.charAt(x) - '0');
        }
        return ans;
    }

}
