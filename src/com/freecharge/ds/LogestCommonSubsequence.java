package com.freecharge.ds;

import java.util.Arrays;

public class LogestCommonSubsequence {
	
	
	public static void main(String[] args) {
		String s1 = "UBZVAFSPQPQ";
		String s2 = "WUZIF";
		int dp[][] = new int[s1.length()][s2.length()];
		for(int arr[]:dp)
			Arrays.fill(arr, -1);
		System.out.println(lcs(s1,s2,s1.length()-1,s2.length()-1,dp));
		
		
	}
	
	
	public static int lcs(String s1,String s2,int m,int n,int dp[][]) {
		if(m<0||n<0)
			return 0;
		if(dp[m][n] !=-1)
			return dp[m][n];
		
		if(s1.charAt(m) == s2.charAt(n)) {
			dp[m][n] =  1+lcs(s1,s2,m-1,n-1,dp);
			return dp[m][n];
		}else {
			dp[m][n] = Math.max(lcs(s1,s2,m-1,n,dp),lcs(s1,s2,m,n-1,dp));
			return dp[m][n];
		}
	}

}
