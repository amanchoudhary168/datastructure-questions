# DataStructure Questions:
This repository contains solution for below mentioned programs in the list.
`For detailed list you can visit below mentioned link
    https://leetcode.com/list/56grpdhd
`

# Question List
1. CountInversions : - It count the inversion in an array. 
2. BussinessNotation : - Program to convert a number in million and K notation.
3. Decode String : - Program to print  all possible ways to decode a encoded numeric string.  
4. LongestSubStringWithoutRepition : - Program to print the longest substring with unique characters. 
. 
